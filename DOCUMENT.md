# Interview Exercise

Implement a server that responds to quote requests.

## Assumptions

1. The number of bytes per clients' request message is less than 1024 bytes;
2. Clients will receive `ERROR: [details]` message if any exceptions 
   (e.g. no quote price is calculated);
3. Negative price should not be used as INVALID as some instruments may have negative price;
4. Only `securityId` as `123`, `456` and `789` are defined; and
5. `quantity` must be a positive integer.

## Description

The server listening to port `8888` could be started by running the following command in terminal:

    python quote_tcp_server.py


The two interfaces (`QuoteCalculationEngine` and `ReferencePriceSource`) with their implementations are located in /marketmarker folder 

* `QuoteCalculationEngine` - its interface is /marketmaker/quote_calculation_engine.py and its implementation is /marketmaker/quote_calculation_engine_impl.py 
* `ReferencePriceSource` - its interface is /marketmaker/reference_price_source.py and its implementation is /marketmaker/reference_price_source_impl.py.


## Testing

Evidence that the server works correctly could be found from `/log/server.log` and `/log/client.log`.

* `123 BUY 23` is valid request
* `123 Buy 23` is INVALID request because the `(BUY|SELL)` must be capital letters
* `123 BUY 23.0` is INVALID request because the `quantity` must be positive integer
* `123 BUY 0` is INVALID request because the `quantity` must be positive integer
* `123 BUY -23` is INVALID request because the `quantity` must be positive integer
* `123 BUY [string]` is INVALID request because the `quantity` must be positive integer
* `[string] BUY [string]` is INVALID request because the `securityId` must be integer
* `234 BUY 23` is INVALID request because the reference price of `securityId`=`234` is not found

