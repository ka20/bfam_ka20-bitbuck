import logging

from tornado.tcpserver import TCPServer
from tornado.netutil import bind_sockets
from tornado.iostream import StreamClosedError
from tornado import gen
from tornado.ioloop import IOLoop

from marketmaker.quote_calculation_engine import QuoteCalculationEngine
from marketmaker.reference_price_source import ReferencePriceSource

from marketmaker.quote_calculation_engine_impl import QuoteCalculationEngineImpl
from marketmaker.reference_price_source_impl import ReferencePriceSourceImpl


logging.basicConfig(level=logging.DEBUG,
                    filename='./log/server.log',
                    filemode='w',
                    format='%(asctime)s %(name)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)


def parse_is_buy(buy_sell_text):
    if buy_sell_text == 'BUY':
        return True
    if buy_sell_text == 'SELL':
        return False
    raise Exception(f'Invalid buy_sell_text (either BUY or SELL only): {buy_sell_text}')

def parse_quantity(quantity_text):
    if quantity_text.isdigit():
        quantity = int(quantity_text)
        if quantity <= 0:
            raise Exception(f'Invalid quantity (positive integer only): {quantity_text}')
        return quantity
    raise Exception(f'Invalid quantity (integer only): {quantity_text}')


class QuoteTcpServer(TCPServer):
    def init_server(self, buffer_size=1024, quote_calculation_engine=None, reference_price_source=None):
        self.buffer_size = buffer_size
        self.quote_calculation_engine = quote_calculation_engine
        self.reference_price_source = reference_price_source

    @gen.coroutine
    def handle_stream(self, stream, address):
        logger.debug('%s connected', address)
        while True:
            try:
                quote_request_bytes = yield stream.read_bytes(self.buffer_size, partial=True)
                quote_request_str = quote_request_bytes.decode('utf-8')
                logger.info('Request: %s', quote_request_str)

                quote_price = self.get_quote_price(quote_request_str)
                quote_price_str = str(quote_price)
                quote_price_bytes = bytes(quote_price_str.encode('utf-8'))
                logger.info('Response: %s', quote_price_str)
                yield stream.write(quote_price_bytes)
            except StreamClosedError:
                logger.debug('%s closed', address)
                del stream
                break
            except Exception as e:
                logger.error('%s exception, %s', address, e)
                err_msg = f'ERROR: {e}'
                logger.error('Response: %s', err_msg)
                yield stream.write(bytes(err_msg.encode('utf-8')))

    def get_quote_price(self, quote_request):
        request_parts = quote_request.split(' ')
        security_id = int(request_parts[0])
        is_buy = parse_is_buy(request_parts[1])
        quantity = parse_quantity(request_parts[2])
        reference_price = self.reference_price_source.get(security_id)
        return self.quote_calculation_engine.calculateQuotePrice(security_id, reference_price, is_buy, quantity)


if __name__ == '__main__':
    port = 8888
    buffer_size = 1024

    quote_calculation_engine_instance = QuoteCalculationEngineImpl()
    reference_price_source_instance = ReferencePriceSourceImpl()

    sockets = bind_sockets(port)
    quote_tcp_server = QuoteTcpServer()
    quote_tcp_server.init_server(buffer_size,
                                 quote_calculation_engine_instance,
                                 reference_price_source_instance)
    quote_tcp_server.add_sockets(sockets)
    IOLoop.current().start()








