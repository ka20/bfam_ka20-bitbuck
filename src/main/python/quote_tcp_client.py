import logging

from tornado import gen
from tornado.tcpclient import TCPClient
from tornado.ioloop import IOLoop


logging.basicConfig(level=logging.DEBUG,
                    filename='./log/client.log',
                    filemode='w',
                    format='%(asctime)s %(name)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)


class QuoteTcpClient(TCPClient):
    def __init__(self, host, port):
        self.host = host
        self.port = port

    @gen.coroutine
    def start(self):
        self.stream = yield TCPClient().connect(self.host, self.port)
        while True:
            yield self.send_message()
            yield self.receive_message()
            pass

    @gen.coroutine
    def send_message(self):
        msg = input('Input: ')
        logger.info('Request: %s', msg)
        bytes_msg = bytes(msg.encode('utf-8'))
        yield self.stream.write(bytes_msg)

    @gen.coroutine
    def receive_message(self):
        try:
            logger.debug('receive data...')
            bytes_msg = yield self.stream.read_bytes(1024, partial=True)
            logger.debug('Response: %s', bytes_msg)
        except Exception as e:
            logger.error('tcp client exception: %s', e)


if __name__ == '__main__':
    client = QuoteTcpClient('127.0.0.1', 8888)
    client.start()
    IOLoop.instance().start()
