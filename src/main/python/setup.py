from setuptools import setup, find_packages


setup(
    name='quote_tcp_server',
    version='1.0.0',
    description='TCP Server for quote price',
    author='Kin Au',
    author_email='knsatrading@gmail.com',
    packages=find_packages(exclude=('tests', 'docs'))
)