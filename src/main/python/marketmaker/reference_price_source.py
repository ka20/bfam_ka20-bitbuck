from marketmaker.reference_price_source_listener import ReferencePriceSourceListener


class ReferencePriceSource:
    def subscribe(self, listener: ReferencePriceSourceListener):
        pass

    def get(self, securityId: int) -> float:
        pass
