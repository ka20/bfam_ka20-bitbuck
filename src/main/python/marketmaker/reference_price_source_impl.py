from random import random
from marketmaker.reference_price_source_listener import ReferencePriceSourceListener
from marketmaker.reference_price_source import ReferencePriceSource


class ReferencePriceSourceImpl(ReferencePriceSource):
    def __init__(self):
        self.price_dict = dict()
        self.price_dict[123] = random()
        self.price_dict[456] = random()
        self.price_dict[789] = random()

    def subscribe(self, listener: ReferencePriceSourceListener):
        pass

    def get(self, securityId: int) -> float:
        if securityId in self.price_dict:
            return self.price_dict[securityId]
        raise Exception(f'No reference price for securityId:{securityId}')

    def start(self):
        # realtime subscribe reference price from external source and
        # update self.price_dict in thread-safe manner
        pass